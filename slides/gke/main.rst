Google K8S Engine
=================

----

Google K8S Engine
=================

.. image:: ../images/gke-big-picture.png
   :width: 80%

----

Google K8S Engine
=================

GKE is a managed, production-ready environment for deploying containerized applications.

----

GKE Architecture
================

.. image:: ../images/gke_k8s.png
   :width: 80%

----

Tools
=====

- Terraform - https://terraform.io/downloads.html
- Kubectl - https://kubernetes.io/docs/tasks/tools/install-kubectl/
- Google Cloud SDK - https://cloud.google.com/sdk

----

Demo Files
==========

.. code-block:: c

    $ tree gke-demo/
    gke-demo/
    ├── gke
    │   ├── cluster.tf
    │   ├── gcp.tf
    │   └── variables.tf
    ├── k8s
    │   ├── k8s.tf
    │   ├── pods.tf
    │   ├── services.tf
    │   └── variables.tf
    └── main.tf

    2 directories, 8 files

----

Demo Time
=========

.. image:: ../images/demo-time-gke.jpg
	:width: 100%

|gke_location_link|

.. |gke_location_link| raw:: html

   <a href="http://localhost:5000" target="_blank">IaC Graph</a>
