References
==========

* Terraform
   - https://www.terraform.io
   - https://www.terraform.io/docs/providers/google/r/container_cluster.html
   - https://www.terraform.io/docs/providers/aws/r/eks_cluster.html

* Google K8s Engine
   - https://cloud.google.com/kubernetes-engine/

* Amazon EKS
   - https://aws.amazon.com/eks/

* This Presentation Slides
   - https://gitlab.com/slackmart/k8s-slides

----

Q & A
=====

----
