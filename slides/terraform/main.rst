Terraform
=========

----

Terraform
=========

.. image:: ../images/terraform.png
   :width: 25%
   :align: center


.. class:: center

   Uses HCL

----

Terraform HCL
=============

How does it look like?
----------------------

.. code-block:: c

   resource "aws_eks_cluster" "guestbook_cluster" {

     name = "${var.cluster_name}"

     role_arn = "${aws_iam_role.demo_eks_cluster_role.arn}"

     vpc_config = {

         security_group_ids = [

             "${aws_security_group.demo_security_group.id}"

         ]

         subnet_ids = ["${aws_subnet.demo_subnet.*.id}"]

     }

   }
