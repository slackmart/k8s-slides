Amazon EKS
==========

----

Amazon EKS
==========

.. image:: ../images/eks-big-picture.png
   :width: 80%

.. notes: kubectl => to support exec authentication with usage of aws-iam-authenticator

.. notes: Requires:
.. notes: - IAM credentials with suitable access to create AutoScaling, EC2, EKS, and IAM resources
.. notes: - A Virtual Private Cloud
.. notes: - kubectl >= 1.10


----

Amazon EKS
==========

Makes it easy to deploy, manage, and scale containerized applications using Kubernetes on AWS.

----

EKS Architecture
================

.. image:: ../images/eks_k8s.png
   :width: 80%

----

Tools
=====

- Terraform - https://terraform.io/downloads.html
- Kubectl - https://kubernetes.io/docs/tasks/tools/install-kubectl/
- aws-iam-authenticator - https://github.com/kubernetes-sigs/aws-iam-authenticator

----

Demo Files
==========

.. code-block:: c

    $ tree eks-demo/
    eks-demo/
    ├── README.rst
    ├── eks
    │   ├── cluster.tf
    │   ├── eks.tf
    │   ├── master.tf
    │   ├── networking.tf
    │   ├── outputs.tf
    │   ├── variables.tf
    │   └── workers.tf
    ├── k8s
    │   ├── k8s.tf
    │   ├── pods.tf
    │   ├── services.tf
    │   └── variables.tf
    └── main.tf

    2 directories, 10 files

----

Demo Time
=========

.. image:: ../images/demo-time-eks.png
   :width: 90%

|eks_location_link|

.. |eks_location_link| raw:: html

   <a href="http://localhost:5001" target="_blank">IaC Graph</a>
