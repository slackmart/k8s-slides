Terraforming K8S
================

----

Ice breaker
===========

Hugo Celedonio Gonzalez Torres
-------------------------------

- 19+ years Experience as sysadmin (now DevOps)
- Baseball, softball, reading, movies
- I have lived in Piedras Negras, Monterrey, Austin TX, Raleigh NC and Chicago Il
- Married, 2 Children: Leonardo and Renata

----

Ice breaker
===========

Martín Rodríguez Guerrero
-------------------------

- 5+ years experience as a backend developer/devops
- Football, biking, programming
- I have lived in Guerrero, CDMX and Monterrey
- Single

----

   "If you think it's expensive to hire a professional, wait until you hire an amateur."

.. image:: images/dou-solo-white.png
   :align: right
   :width: 25%
