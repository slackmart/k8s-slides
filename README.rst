Terraforming Kubernetes
=======================

Getting started
---------------

::

    $ python3 -m venv /tmp/k8s_slides_env
    $ . /tmp/k8s_slides_env/bin/activate
    (/tmp/k8s_slides_env) $ pip install -r requirements.txt
    (/tmp/k8s_slides_env) $ darkslide config.cfg

Open presentation.html using your preferred web browser

Adding slides' sources
----------------------

Open config.cfg and add it in the source section, one file per line.

Editing slides
--------------

Supported source: formats_

.. _formats: https://github.com/ionelmc/python-darkslide#formatting

Visualizating demo's graphs
---------------------------

::

    (/tmp/k8s_slides_env) $ git submodule update --init --recursive --remote
    (/tmp/k8s_slides_env) $ sh run_blastradius.sh

Open localhost:5000 (for the gke demo), and localhost:5001 (for the eks demo) using your preferred web browser
