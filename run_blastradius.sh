function init {
    pushd $1
    terraform init
    popd
}

init "gke-demo"
init "eks-demo"

nohup blast-radius --port 5000 --serve gke-demo/ &
nohup blast-radius --port 5001 --serve eks-demo/ &
